import * as BABYLON from "babylonjs";
export declare class CameraMoveLink {
    link: HTMLElement;
    camera: BABYLON.FollowCamera;
    object?: BABYLON.Mesh;
    constructor(link: HTMLElement, camera: BABYLON.FollowCamera, object?: BABYLON.Mesh);
    moveToPosition(): void;
}
