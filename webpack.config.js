const path = require('path');
module.exports = {
  mode: 'development',
  entry: {
    index: './src/scripts/index.ts',
    contentToggle: './src/scripts/components/contenthide.ts',
    cameraMoveLink: './src/scripts/components/cameramovelink.ts'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'ts-loader',
    }]
  }
};