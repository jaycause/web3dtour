import { Engine, Scene, FollowCamera, HemisphericLight, Vector3, Mesh, Color3, StandardMaterial } from "babylonjs";
import { GridMaterial } from "babylonjs-materials";
import { CameraMoveLink } from "./components/cameramovelink"

const canvas = document.getElementById("renderCanvas") as HTMLCanvasElement;
const engine = new Engine(canvas);
var scene = new Scene(engine);
var light = new HemisphericLight("main-light", new Vector3(0, 1, 0), scene);
light.intensity = 0.7;

var material = new GridMaterial("grid", scene);
var mat1 = new StandardMaterial("std1", scene);
var mat2 = new StandardMaterial("std2", scene);
var mat3 = new StandardMaterial("std3", scene);
var mat4 = new StandardMaterial("std4", scene);
var clearMat = new StandardMaterial("clearMat", scene);
var sphere = Mesh.CreateSphere("sphere", 16, 2, scene);
var cube = Mesh.CreateBox("cube", 1, scene);
var icoSphere = Mesh.CreateIcoSphere("icoSphere", {}, scene);
var torusKnot = Mesh.CreateTorusKnot("torusKnow", 1, 0.5, 32, 32, 2, 3, scene);
var originCube = Mesh.CreateBox("origin", 1, scene);

sphere.position = new Vector3(0, 2, -10);
sphere.material = mat1;
mat1.diffuseColor = new Color3(1, 1, 0);

cube.position = new Vector3(10, 2, 0);
cube.material = mat2;
mat2.diffuseColor = new Color3(1, 0, 0);

icoSphere.position = new Vector3(0, 2, 10);
icoSphere.material = mat3;
mat3.diffuseColor = new Color3(0, 1, 0);

torusKnot.position = new Vector3(-10, 2, 0);
torusKnot.material = mat4;
mat4.diffuseColor = new Color3(0, 0, 1);

originCube.position = new Vector3(0, 10, 12);
originCube.material = clearMat;
clearMat.alpha = 0;

var camera = new FollowCamera("main-camera", new Vector3(0, 30, 20), scene);

var ground = Mesh.CreateGround("ground", 512, 512, 32, scene);
ground.material = material;

const cubeBtn = new CameraMoveLink(
    document.getElementById("cube-btn"),
    camera,
    cube
);
const sphereBtn = new CameraMoveLink(
    document.getElementById("sphere-btn"),
    camera,
    sphere
);
const icoSphereBtn = new CameraMoveLink(
    document.getElementById("icosphere-btn"),
    camera,
    icoSphere
);
const torusBtn = new CameraMoveLink(
    document.getElementById("torus-btn"),
    camera,
    torusKnot
);

const originBtn = new CameraMoveLink(
    document.getElementById("home-btn"),
    camera,
    originCube
);

camera.radius = 5;
camera.rotationOffset = 0;
camera.heightOffset = 5;
camera.maxCameraSpeed = 2;
camera.cameraAcceleration = 0.025;
camera.attachControl(canvas, true);
camera.lockedTarget = originCube;

engine.runRenderLoop(() => {
    scene.render();
});

//  Show debug inspector
// scene.debugLayer.show();