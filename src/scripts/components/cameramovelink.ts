import * as BABYLON from "babylonjs";

export class CameraMoveLink {
    link: HTMLElement
    camera: BABYLON.FollowCamera
    object?: BABYLON.Mesh
    constructor (link: HTMLElement, camera: BABYLON.FollowCamera, object?: BABYLON.Mesh) {
        this.link = link;
        this.camera = camera;
        this.object = object;
        this.link.onclick = (e) => { this.moveToPosition(); }
    }
    moveToPosition () {
        this.camera.lockedTarget = this.object;
    }
}