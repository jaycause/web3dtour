class ToggleButton {
    button: HTMLElement
    constructor () {
        this.button = document.getElementById("content-hide");
        this.button.onclick = (e) => { this.toggleContent(); }
    }
    toggleContent () {
        let contentBlock = document.getElementById("web-content");
        contentBlock.style.visibility = 
            contentBlock.style.visibility === "visible" ? "hidden" : "visible";
    }
}

const btn = new ToggleButton();